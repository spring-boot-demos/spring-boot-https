# spring-boot project configured with Https protocol

This a demo project to configure a spring-boot project with https protocol.

## Tools and software's used
* Java  : open-jdk-8
* Maven : 3.6
* Git   : 2.17.1
* IDE   : IntelliJ ce

## How to build  
* Import project into local workspace
* run command : mvn clean install

## How to run
* For testing you can run via IDE
* Or run the command : java -jar target/spring-boot-https-demo-0.0.1-SNAPSHOT.jar

## How to test

Http test : use the url  http://localhost:8333/hello and put it in any web browser.
You should get the message :  
"Bad Request - This combination of host and port requires TLS." 

Https test : use the url  http://localhost:8333/hello
Response should be : Hello via Https


